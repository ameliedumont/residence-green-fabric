%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: -1 -1 52 86 
%%HiResBoundingBox: -0.5 -0.5 51.5237 85.53935 
%%Creator: MetaPost 2.01
%%CreationDate: 2023.10.18:1440
%%Pages: 1
%%DocumentResources: procset mpost-minimal
%%DocumentSuppliedResources: procset mpost-minimal
%%EndComments
%%BeginProlog
%%BeginResource: procset mpost-minimal
/bd{bind def}bind def/fshow {exch findfont exch scalefont setfont show}bd
/fcp{findfont dup length dict begin{1 index/FID ne{def}{pop pop}ifelse}forall}bd
/fmc{FontMatrix dup length array copy dup dup}bd/fmd{/FontMatrix exch def}bd
/Amul{4 -1 roll exch mul 1000 div}bd/ExtendFont{fmc 0 get Amul 0 exch put fmd}bd
/ScaleFont{dup fmc 0 get Amul 0 exch put dup dup 3 get Amul 3 exch put fmd}bd
/SlantFont{fmc 2 get dup 0 eq{pop 1}if Amul FontMatrix 0 get mul 2 exch put fmd}bd
%%EndResource
%%EndProlog
%%BeginSetup
%%EndSetup
%%Page: 1 1
 0 0 0 setrgbcolor 1 0 dtransform exch truncate exch idtransform pop setlinewidth
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 0 85.03935 moveto
0 0 lineto stroke
 0 1 dtransform truncate idtransform setlinewidth pop
newpath 11.33841 85.03935 moveto
51.0237 85.03935 lineto stroke
newpath 11.33841 42.51968 moveto
51.0237 42.51968 lineto stroke
newpath 51.0237 0 moveto
11.33841 0 lineto stroke
showpage
%%EOF
