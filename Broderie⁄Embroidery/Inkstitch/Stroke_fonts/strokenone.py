import fontforge
import glob
import lxml.etree as ET

#change here the path to your SVG glyphs folder
svg_final = glob.glob('ductus_sources/ductus-regular/svg' + '/*svg')
print(svg_final)

# change the style of the stroke for each svg letter to none, otherwise the svg font will not have open strokes
for files in svg_final:
    with open(files, 'rt') as f:
        tree = ET.parse(f)
    root = tree.getroot()
    new_style = "stroke:none;"
    root.set('width', '350')
    root.set('height', '550')
    root.set('viewBox', '0 0 350 550')
    for path in root.iter():
        style = path.attrib.get('style')
        if style:
            path.set('style', new_style)

    letter_svg = files.split("/")[-1].replace(".svg", "")
    tree.write(letter_svg + '.svg')
