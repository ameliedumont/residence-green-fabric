import os
import fontforge

#modify here the path to the folder of your svg glyphs
LETTERS_DIR = "ductus_sources/ductus-regular/svg_final"
#create an empty font file with Fontforge before running this script
BLANK_FONT = "blank.sfd"

letters = []

for subdir, dirs, files in os.walk(LETTERS_DIR):
    for file in files:
        new_file = file.split("/")[-1].replace(".svg", "")
        letters.append(new_file)
print(letters)

# open a blank font template
font = fontforge.open(BLANK_FONT)

#import the svg outlines for each letter
for letter in letters:
    font.createChar(int(letter))
    print(letter)
    font[int(letter)].importOutlines(LETTERS_DIR + "/" + letter + ".svg")

    font[int(letter)].left_side_bearing = 65
    font[int(letter)].right_side_bearing = 65

#generate the svg font
font.generate("ductus-regular-stroke.svg")


