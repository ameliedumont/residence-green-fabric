(ENGLISH BELOW)

# Broderie

Dans mes recherches concernant la typographie pour la broderie numérique, j'ai travaillé le dessin des caractères sous l'angle du dessin vectoriel. Il est possible de broder de la typographie par remplissage de lettres existantes, cependant j'ai choisi de laisser de côté cette approche car je la trouvais limitée, en terme de forme et de points de broderie possibles. J'ai plutôt pris le parti de traiter le dessin des caractères par un tracé central (stroke fonts), de telle sorte que le brodeuse ne passe qu'une seule fois sur chaque tracé qui compose une lettre. La machine à broder a alors un rôle comparable à celui d'un plotter ou d'un stylo, qui tracerait des caractères par leur tracé central, mais avec du fil sur du tissu. 

Vous trouverez ci-dessous la documentation de mes essais avec différents outils numériques permettant de traiter la typographie de la manière décrite ci-dessus.

## Ink/Stitch (Inkscape)

[Ink/Stitch](https://inkstitch.org/) est une extension Inkscape permettant d'exporter des fichiers de broderie à partir de dessins vectoriels. Voir cette page pour l'installation de l'extension dans Inkscape : https://inkstitch.org/fr/docs/install/

### Découverte des points de broderie

Ma prise en main d'Ink/Stitch a commencé par la découverte des points, afin de comprendre comment les tracés vectoriels pouvaient être convertis en points de broderie. Sachant que mon but était de traiter la typographie par le squelette central, je me suis concentrée sur le tracé de lignes simples et de contours et ai laissé de côté les propriétés de remplissage de formes de cette extension Inkscape.

- Point droit

Le point droit est le point de broderie le plus basique : la machine passe une seule fois sur le tracé. On peut paramétrer la distance entre les points ainsi que le seuil de tolérance de l'espacement. Si ce seuil est élevé, les courbes vont être découpées de manière assez importante. 

![Point droit](Inkstitch/Images/inkstitch01.png "Point droit")

- Point triple

Avec le point triple, l'aiguille repasse plusieurs fois  sur le même point, ce qui donne une épaisseur au trait. On peut alors augmenter la graisse du tracé en paramétrant un nombre de passages plus important sur chaque point.

![Point triple](Inkstitch/Images/inkstitch02.png)

- Point de satin

Le point de satin est un remplissage par-dessus un tracé. Par défaut ce remplissage est perpendiculaire au tracé, mais avec Ink/Stitch on peut paramétrer la pente selon laquelle le point de satin va être effectué par la machine. On peut évidemment aussi faire varier la largeur de ce point et donc l'épaisseur de trait appliquée sur le tracé. Ink/Stitch permet également d'appliquer des effets de chemin directement avec un point de satin.

![Point de satin](Inkstitch/Images/inkstitch03.png)

Ce premier temps de découverte des fonctionnalités d'Ink/Stitch m'a permis de mieux cerner les résultats que je pourrais obtenir en brodant des lettres par un tracé central. 

### Extension Texte Hershey

Inkscape possède par défaut une extension Texte Hershey, qui permet de composer du texte avec des stroke fonts ouvertes (les tracés des caractères ne sont pas fermés, ce qui est particulièrement utile pour écrire avec un plotter par exemple, qui ne va passer sur les tracés qu'une seule fois). Cela fonctionne également pour la broderie, ci-dessous un essai de texte brodé avec des fontes Hershey. L'avantage d'utiliser cette extension est qu'on peut passer directement de la composition de texte à la broderie.

![Fontes Hershey](Inkstitch/Stroke_fonts/Images/inkstitch04.png)

Par défaut, l'extension ne contient que les fontes Hershey, mais il est possible d'ajouter ses propres stroke fonts aux fichiers de l'extension pour pouvoir composer du texte avec. Pour cela il faut exporter sa stroke font au format SVG, avec des tracés ouverts. Pour exporter une fonte avec des tracés ouverts, voir [cette partie](https://github.com/isdat-type/Relief-SingleLine/tree/main#fontsopen_svg) de la documentation du projet Relief Single Line. Une fois la fonte SVG correctement formatée, il faut aller la déposer dans le dossier de l'extension Hershey d'Inkscape pour pouvoir l'utiliser. Selon le système d'exploitation, ce dossier se situe à un emplacement différent :

- Linux (Ubuntu) :

    > /usr/share/inkscape/extensions/svg_fonts

- Mac OS :

    > /Applications/Inkscape.app/Contents/Resources/share/inkscape/extensions/svg_fonts

- Windows :

    > C:\Program Files\Inkscape\share\inkscape\extensions\svg_fonts

Ci-dessous deux broderies réalisées avec des stroke fonts ajoutées dans l'extension Hershey : la [Relief Single Line](https://github.com/isdat-type/Relief-SingleLine/tree/main) et la [JanStroke](https://gitlab.constantvzw.org/osp/workshop.single-line/-/tree/master/Fonts?ref_type=heads). À noter que les effets de chemin avec le point de satin présentent des bugs : sur un glyphe composé de plusieurs chemins, un seul sera pris en compte lors de l'export en fichier de broderie. Par contre, l'application d'effets de chemin d'Inkscape avant de transformer le tracé en fichier de broderie fonctionne bien. C'est bien le chemin modifié qui sera brodé.

![Relief Single Line](Inkstitch/Stroke_fonts/Images/inkstitch05.png)

![Janstroke](Inkstitch/Stroke_fonts/Images/inkstitch06.png)

J'ai écrit des scripts en Python (**strokenone.py** et **svg2svgfont.py**) qui permettent de générer une fonte SVG à partir d'un ensemble de glyphes eux aussi dessinés en SVG. La marche à suivre est la suivante si vous voulez utiliser ces scripts :

*Attention en l'état ces scripts sont très expérimentaux et ne supportent pas tous les glyphes, uniquement les capitales et minuscules de l'alphabet latin*

1. Il faut tout d'abord réunir ses glyphes SVG dans un dossier (le plus simple est de les mettre dans le même dossier que les scripts Python)
2. On va utiliser en premier le script **strokenone.py**, qui va attribuer la propriété "stroke: none" à chacun des chemins de chaque lettre SVG. Si les chemins SVG n'ont pas cette propriété avant d'utiliser le deuxième script, ils ne seront pas importés comme des chemins ouverts (ce qui est précisément ce qu'on recherche ici). Pour l'utiliser, il faut ouvrir un terminal dans le dossier du script (ou naviguer dans le terminal jusqu'à ce dossier) et lancer la commande :

    > python3 strokenone.py
    
3. Le script va générer de nouveaux fichiers SVG (vous pouvez vérifier en les ouvrant avec un éditeur de texte que les tracés possèdent bien la propriété "stroke: none"). Placez-les dans un nouveau dossier. Dans le script **svg2svgfont.py**, il faut modifier à la ligne 5 le chemin pour qu'il pointe vers ce nouveau dossier avec les SVG sans stroke. Ouvrez [Fontforge](https://fontforge.org/en-US/) et créez un ficher fonte vide que vous nommerez **blank.sfd** avant de l'enregistrer. Ce fichier **blank.sfd** doit se trouver dans le même dossier que les scripts Python. Quand ce fichier fonte vide est créé, on peut lancer le deuxième script avec la commande :

    > python3 svg2svgfont.py
    
4. Un fichier fonte SVG est généré par le script. À partir de là, il faut procéder comme on l'a vu juste au-dessus pour ouvrir les tracés de la fonte SVG (voir [cette partie](https://github.com/isdat-type/Relief-SingleLine/tree/main#fontsopen_svg) de la documentation du projet Relief Single Line) et venir la placer dans les fichiers de l'extension d'Inkscape.

Voici par exemple ci-dessous la fonte [Ductus Regular](https://gitlab.com/ameliedumont/fonts/-/tree/master/Ductus/regular?ref_type=heads) transformée en fonte SVG à l'aide de ces deux scripts, importée dans l'extension Hershey et brodée. L'intérêt de cette méthode est qu'on récupère le squelette des lettres, sur lequel on peut appliquer les points de broderie de son choix et jouer sur des paramètres tels que l'espacement des points pour simplifier les tracés.

![Ductus Regular](Inkstitch/Stroke_fonts/Images/inkstitch07.png)

On peut voir sur cette capture d'écran comment les paramètres de broderie peuvent être réglés sur les tracés d'une lettre :

![Inkstitch](Inkstitch/Stroke_fonts/Images/inkstitch09.png)

La même méthode appliquée à la fonte [Ductus Mono](https://gitlab.com/ameliedumont/fonts/-/tree/master/Ductus/monospace?ref_type=heads) :

![Ductus Mono](Inkstitch/Stroke_fonts/Images/inkstitch08.png)

## Pembroider (Processing)

[Pembroider](https://github.com/CreativeInquiry/PEmbroider) est une librairie [Processing](https://processing.org/) qui permet d'exporter ses sketchs dans des formats de fichiers standards pour les machines à broder numériques. J'ai voulu tester cette librairie pour guider ma recherche davantage vers le dessin de caractères après avoir exploré les possibilités offertes par les stroke fonts dans Inkscape. Cette piste est très intéressante, mais on utilise des fontes déjà existantes, ou du moins pensées en amont du projet de broderie, sur lesquelles on vient appliquer des paramètres de broderie. Je voulais m'essayer à des outils permettant de dessiner et d'exporter directement en fichier à broder des caractères.

Avec Pembroider, on utilise les fonctions de dessin de Processing pour tracer des lettres, c'est une approche différente. Pour installer l'extension, il faut télécharger ses fichiers depuis [ici](https://github.com/CreativeInquiry/PEmbroider/blob/master/distribution/PEmbroider/download/PEmbroider_for_Processing_4.zip), puis aller dans son dossier Processing : **sketchbook** > **libraries**, créer un nouveau dossier **Pembroider** et venir y glisser les fichiers que l'on vient de télécharger. Si la librairie est bien installée, on la trouve lorsqu'on ouvre Processing dans **Sketch** > **Importer une librairie**.

Là encore, sachant que je travaille la typographie par un tracé central plutôt que par un contour, je me suis concentrée sur les fonctions relatives au tracé de contour et n'ai pas fait appel aux fonctions de remplissage de formes. On peut trouver [ici](https://github.com/CreativeInquiry/PEmbroider/blob/master/PEmbroider_Cheat_Sheet.md#strokes) la documentation des fonctions de tracé des contours de Pembroider. La librairie possède une fonction d'import de texte, mais je ne l'ai pas utilisée car cela revient à importer des typographies existantes, dessinées par le contour, et à effectuer un remplissage dessus. À noter que les stroke fonts ouvertes ne fonctionnent pas dans Pembroider.

J'ai trouvé particulièrement intéressantes certaines des fonctions de cette librairie. Tout d'abord le **strokeMode**, qui détermine comment les points de broderie vont se placer par rapport au contour. En mode **PERPENDICULAR**, les points seront comme le nom l'indique positionnés perpendiculairement au tracé, c'est un point de satin assez classique. En mode **TANGENT**, les points de broderie seront placés autour du tracé de base (on peut voir ce que cela donne sur la première ligne de la capture d'écran ci-dessous), ce qui est une fonctionnalité intéressante graphiquement. On peut ensuite jouer avec l'espacement entre les points et la largeur du point pour obtenir différents résultats.

![Pembroider](Pembroider/Images/pembroider03.png)

Le résultat brodé de ce sketch. J'ai utilisé des boucles de programmation pour faire varier soit le dessin des lettres, soit les points de broderie, soit les deux à la fois :

![Pembroider](Pembroider/Images/pembroider02.png)

Une autre fonctionnalité que j'ai exploré avec cet outil est le **strokeLocation**, qui permet de déterminer si on veut se placer, à l'intérieur, pile sur le tracé ou encore à l'extérieur de celui-ci. On peut voir sur la première ligne de la broderie ci-dessous comment la valeur attribuée à cette propriété fait fortement varier l'aspect d'une même forme de base.

![Pembroider](Pembroider/Images/pembroider01.png)

Après y avoir consacré du temps dans le cadre de mes recherches, je recommande cet outil davantage pour du lettrage que pour réellement créer une typographie avec laquelle on pourrait composer du texte, ce qui serait vraiment fastidieux car il faudrait créer une fonction pour chaque lettre puis gérer un système de composition de texte. Mais pour dessiner un lettrage à broder, l'outil est très intéressant, et il l'est tout particulièrement pour son aspect programmatique/génératif. Il y a un potentiel de création de lettres variables selon différents paramètres qui reste à explorer.

## Turtle Stitch

[Turtle Stitch](https://www.turtlestitch.org/run) est un outil utilisant le langage de programmation visuelle Scratch, sur lequel ont été ajoutées des fonctions de broderie. Il permet de dessiner des formes directement avec des points de broderie dont on peut gérer les paramètres. C'est un outil en ligne, on compose son dessin en positionnant des blocs dans l'espace de travail ; on définit une suite d'instructions que le programme va interpréter. Il est aussi possible de définir ses propres fonctions, des blocs de code qu'on peut réutiliser autant qu'on le veut, comme on peut le voir ci-dessous :

![Turtle Stitch](Turtle_Stitch/Images/turtlestitch04.png)

J'ai donc commencé à définir une fonction pour chaque lettre de l'alphabet latin, qui défnit le tracé de base du glyphe. Rapidement, une fonctionnalité de Turtle Stitch m'a paru particulièrement pertinente dans le cadre de mes recherches sur la typographie pour la broderie. On dessine avec un curseur qui suit les instructions qu'on lui donne. Lorsque la forme a été tracée, le curseur reste à la position où il a terminé le dessin plutôt que de repartir à une origine absolue. J'ai trouvé cela intéressant pour faire en sorte de broder plusieurs lettres à la suite, écrire des mots et même pouvoir composer du texte plus largement. Afin de faciliter quelque peu mon travail de dessin de caractères et de m'assurer que je repartais toujours de la même ligne de base, j'ai choisi d'adopter une contrainte : toujours faire terminer le tracé des caractères en bas à droite d'une boîte invisible qui les engloberait. 

J'en suis arrivée à une première version de cette typographie, basée sur la contrainte évoquée ci-dessus. J'ai également mis en place un paramètre variable permettant de modifier l'échelle des caractères, et il est toujours possible de modifier le point de broderie qu'on veut utiliser.

![Turtle Stitch](Turtle_Stitch/Images/turtlestitch02.png)

Cette première version m'a amenée à penser la typographie que je dessinais dans Turtle Stitch comme un système d'écriture cursive, et comment faire en sorte que les lettres s'enchainent de manière fluide. Ensuite je me suis vraiment penchée sur la composition de texte : comment gérer les espaces et revenir à la ligne. La capture d'écran suivante montre comment écrire directement le texte à broder : on fait appel aux fonctions de chaque lettre dans l'ordre selon lequel on veut les faire apparaitre. Le retour à la ligne est pour le moment géré en se déplaçant dans la zone de travail mais il pourrait être automatisé.

![Turtle Stitch](Turtle_Stitch/Images/turtlestitch05.png)

Je trouvais intéressant de faire en sorte que la brodeuse numérique ne coupe pas le fil (ou aussi peu que possible) entre les caractères, alors j'ai exploité cette contrainte dans le dessin des lettres. Voici un premier texte composé :

![Turtle Stitch](Turtle_Stitch/Images/turtlestitch01.png)

Et un deuxième texte brodé, plus long, pour éprouver mon système d'écriture et voir s'il fonctionnait correctement. Il s'agit d'un extrait du *Cyborg Manifesto* de Donna Haraway :

![Turtle Stitch](Turtle_Stitch/Images/turtlestitch03.png)

Bien que Turtle Stitch puisse sembler un peu plus sommaire que d'autres outils (notamment parce que ses fonctions de dessin sont réduites à quelques figures géométriques de base, ce qui rend parfois le dessin peu précis), je pense que c'est un outil très intéressant et j'ai apprécié d'en exploiter les spécificités pour en faire émerger une création typographique propre à ce langage et à son interface. Le projet n'est pas terminé, il faudrait poursuivre le dessin des caractères pour avoir un set complet et vraiment pouvoir composer n'importe quel texte. Mais il me semble que les prototypes qui sont sortis de ce projet à l'heure actuelle font émerger une proposition originale qui mérite d'être poursuivie.

—

# Embroidery

In the frame of my researches on typography for digital embroidery, I chose to work on type design with vectorial drawing. It is possible to embroider typography by filling existing letters, but I chose not to work this way because I found it too limited, both in terms of shape design and embroidery stitches. Instead I chose for this project to treat type design by a central path (this is what we call stroke fonts), so that the embroidery machine goes only once on each path of a letter. The embroidery machine then has a function that can be compared to what a pen plotter does, which draws characters by their center line, but with thread on fabric.

You will find below the documentation of my researches on several digital tools, looking for ways to design typography the way I just described.

## Ink/Stitch (Inkscape)

[Ink/Stitch](https://inkstitch.org/) is an Inkscape extension made for exporting embroidery files from vectorial drawings. See on this page how to install the extension : https://inkstitch.org/fr/docs/install/

### Discovery of the embroidery stitches

My tests on Ink/Stitch started with discovering the different stitches, to understand how do vectorial paths could be converted into embroidery stitches. Considering that my goal was to treat typography by a central line, I focused on how to draw simple lines and strokes, and didn't use the shape filling properties of this Inkscape extension. 

- Running stitch

Running stitch is the most basic embroidery stitch : the machine goes only once along the path. We can set the distance between stitches and the tolerance threshold applied on this spacing. If this threshold is set to a high value, the curves will be sharpened in a quite instense way.

![Point droit](Inkstitch/Images/inkstitch01.png "Point droit")

- Bean stitch

With bean stitch, the needle comes back and forth several times on the same stitch, which gives the stroke a higher thickness. We can then make the stroke thickness even bigger by setting a higher number of repetitions on the stitch.

![Point triple](Inkstitch/Images/inkstitch02.png)

- Satin column

Satin column is a filling over a path. By default the stitch is perpendicular to the path, but with Ink/Stitch we can set the direction of the satin column the machine will embroider. We can of course change the width of the stitch, and so the thickness of the stroke applied on the path. Ink/Stitch also enables to apply path effects directly on a satin column.

![Point de satin](Inkstitch/Images/inkstitch03.png)

This first discovery time of Ink/Stitch features helped me understanding better what kind of results I could get by embroidering letters with a central path.

### Hershey text extension

Inkscape comes by default with an extension called Hershey Text, which enables typesetting with open stroke fonts (the paths of the glyphs are not closed, which is particularly useful for pen plotting for instance: the machine will go through the paths only once). This also works for embroidery, below is a test of text embroidery with Hershey fonts. The benefit of using this extension is that we can do typesetting and export it for embroidery straight.

![Fontes Hershey](Inkstitch/Stroke_fonts/Images/inkstitch04.png)

By default, the extension only contains Hershey fonts, but it is possible to add your own stroke fonts to the extension files so that you can use it. To do so, you have to export your stroke font to SVG format, with open paths. To know how to export a font with open paths, please read [this part](https://github.com/isdat-type/Relief-SingleLine/tree/main#fontsopen_svg) of the Relief Single Line project. Once the SVG font is properly formated, you have to place it into the Inkscape extension folder to be able to use it. According to your operating system, this folder can be found here:

- Linux (Ubuntu) :

    > /usr/share/inkscape/extensions/svg_fonts

- Mac OS :

    > /Applications/Inkscape.app/Contents/Resources/share/inkscape/extensions/svg_fonts

- Windows :

    > C:\Program Files\Inkscape\share\inkscape\extensions\svg_fonts

Below are two embroideries using stroke fonts added to the Hershey extension: [Relief Single Line](https://github.com/isdat-type/Relief-SingleLine/tree/main) and [JanStroke](https://gitlab.constantvzw.org/osp/workshop.single-line/-/tree/master/Fonts?ref_type=heads). Please note that path effects with satin column have bugs: on a glyph made of several paths, only one of those paths will be taken into account when exporting the embroidery file. But applying Inkscape path effects before converting the path into an embroidery file works fine. The modified path will be embroidered.

![Relief Single Line](Inkstitch/Stroke_fonts/Images/inkstitch05.png)

![Janstroke](Inkstitch/Stroke_fonts/Images/inkstitch06.png)

I wrote Python scripts (**strokenone.py** and **svg2svgfont.py**) that can be combined to generate an SVG font from a set of SVG glyphs. If you would like to use it, you have to do so:

*Disclaimer: for now those scripts are very experimental and do not support all glyphs, only capital and lowercase latin alphabet*

1. You have to gather your SVG glyphs into a folder (it is more simple to create this folder in the same folder as the scripts)
2. We will first use the **strokenone.py** script, which will set the property "stroke: none" on every path of every SVG letter in your folder. If the SVG paths do not have this property before using the second script, it will not be imported as open paths (which is precisely what we are looking for). To use the script, open a terminal in the folder where is the script and run:

    > python3 strokenone.py
    
3. The script is going to generate new SVG files (you can check if the strokes have the property "stroke:none" by opening the files into a text editor). Place those files into a new folder. In the **svg2svgfont.py**, you have to modify the path on line 5 to make it point to your folder with the new SVG glyphs. Open [Fontforge](https://fontforge.org/en-US/) and create an empty font file that you will save as **blank.sfd**. This file **blank.sfd** has to be placed into the same folder as the Python scripts. When the empty font file is created, you can run the second script with the command:

    > python3 svg2svgfont.py

4. A SVG font file is generated by the script. From now on, you have to proceed as explained just before to open the paths of the SVG font (see [this part](https://github.com/isdat-type/Relief-SingleLine/tree/main#fontsopen_svg) of Relief Single Line project documentation) and then place the font into the Inkscape extension files.

This is for instance the font [Ductus Regular](https://gitlab.com/ameliedumont/fonts/-/tree/master/Ductus/regular?ref_type=heads) turned into a SVG font with those two scripts, imported into the Hershey extension and embroidered. What's interesting with using this process is that we get the glyphs skeletons on which we can then apply any embroidery stitch we like and play with parameters such as spacing between the stitches to simplify the paths.

![Ductus Regular](Inkstitch/Stroke_fonts/Images/inkstitch07.png)

We can see on this screenshot how it is possible to set the embroidery parameters on a letter's paths:

![Inkstitch](Inkstitch/Stroke_fonts/Images/inkstitch09.png)

And the same process applied on the font [Ductus Mono](https://gitlab.com/ameliedumont/fonts/-/tree/master/Ductus/monospace?ref_type=heads) :

![Ductus Mono](Inkstitch/Stroke_fonts/Images/inkstitch08.png)

## Pembroider (Processing)

[Pembroider](https://github.com/CreativeInquiry/PEmbroider) is a [Processing](https://processing.org/) library made for exporting sketches into standard file formats for digital embroidery machines. I wanted to discover this library to lead my research more towards type design after exploring the possibilities offered by stroke fonts in Inkscape. This research track is very interesting, but we use already existing fonts or at least fonts that were designed before making an embroidery project with it, and we apply embroidery parameters on it. I wanted to try tools that enable at the same time to draw letters and to export an embroidery file from it.

With Pembroider, we use Processing's drawing functions to draw letters, it is another approach. To install the extension, you have to download the files from [here](https://github.com/CreativeInquiry/PEmbroider/blob/master/distribution/PEmbroider/download/PEmbroider_for_Processing_4.zip), then move to the Processing folder in your system: **sketchbook** > **libraries**, make a new folder **Pembroider** and drop here the library files you just downloaded. If the library is correctly installed, you will see it into your Processing window: **Sketch** > **Manage libraries**.

In this case again, as I work on typography by the central line rather than by the outline, I focused on the functions relative to stroke drawing and didn't make use of the shape filling functions. The documentation of those Pembroider stroke drawing functions can be found [here](https://github.com/CreativeInquiry/PEmbroider/blob/master/PEmbroider_Cheat_Sheet.md#strokes). The library has a text import function, but I didn't use it because all it does is an import of existing fonts, rendered by their outlines, and the shapes are filled. Note: open stroke fonts do not work into Pembroider.

I found some of the functions from this library particularly interesting. First of all the **strokeMode** function, which determines how to position the embroidery stitches on the drawing's stroke. In **PERPENDICULAR** mode, the stitches will (as the name says) be positioned in a perpendicular way to the stroke, this is a basic satin column. In **TANGENT** mode, the stitches will be positioned around the basic stroke (we can see what it does on the first line of the screenshot below), which is a graphically interesting feature. We can then play with the spacing between stitches and the stitch length to get different results.

![Pembroider](Pembroider/Images/pembroider03.png)

Here the embroidery that resulted from this sketch. I used programming loops to create variations on the letters design or on the embroidery stitches, or both at the same time:

![Pembroider](Pembroider/Images/pembroider02.png)

An other feature from this library that I explored is the **strokeLocation**, which enables to set whether we want to position the stitches inside, exactly on or outside of the stroke. We can see on the first line of the embroidery below how the value set for this property modifies a lot how a shape is rendered.

![Pembroider](Pembroider/Images/pembroider01.png)

After spending some time on it in the frame of my research, I recommand using this tool more for lettering than for designing a typography that could be used for typesetting, this would be very tedious because it would require one function per letter and then other functions to make the typesetting system functional. But to design a lettering for embroidery the tool is very interesting, particularly for its programming/generative aspect. There is a potential for making variable letters based on several parameters that could be explored.

## Turtle Stitch

[Turtle Stitch](https://www.turtlestitch.org/run) is a tool based on the visual programming language Scratch, on which embroidery features were implemented. It enables to draw shapes directly with embroidery functions we can parameter in different ways. It is an online tool, we make our drawing by positioning code blocks into the workspace; we define a set of instructions that the program will interpret. It is also possible to define your own functions, code blocks you can reuse whenever you need it, as you can see below:

![Turtle Stitch](Turtle_Stitch/Images/turtlestitch04.png)

So I started defining a function for each letter from the basic latin alphabet, it defines the basic drawing of the glyph. Quickly one Turtle Stitch's feature appeared to be very relevant to me for my research on typography for embroidery. We draw with a cursor that follows the given instructions. Once the shape is drawn, the cursor remains where it ended drawing rather than coming back to an absolute origin. I found this very interesting to find a way to embroider several letters in a row, write words and even do a typesetting work. To make the type design work a little easier to me and also make sure I would always start drawing on the same baseline, I chose to work with a constraint: always end the letter drawing in the right bottom corner of an invisible box it would fit in.

This brought me to a first version of this typography, designed with the constraint I just explained. I also set a variable parameter to modify the scale of the characters, and it is always possible to modify the embroidery stitch used.

![Turtle Stitch](Turtle_Stitch/Images/turtlestitch02.png)

This first version led me to think this typography I was designing in Turtle Stitch as a cursive writing system, and how to make the transition between the letters smooth. Then I really focused on the typesetting part: how to manage spacing and line breaks. The screenshot below shows how to write to text to embroider: we call the functions to draw every letter in the order we want it to appear. Line breaking is for now managed by moving in the workspace but this could be automatised.

![Turtle Stitch](Turtle_Stitch/Images/turtlestitch05.png)

I found interesting to have the embroidery machine not cutting the thread (or as few as possible) between characters, so I exploited this constraint in the letter design. Here is a first text made with the tool:

![Turtle Stitch](Turtle_Stitch/Images/turtlestitch01.png)

And a second embroidered text, longer, to test my writing system and see if it functioned correctly. The text is an extract from the *Cyborg Manifesto* by Donna Haraway :

![Turtle Stitch](Turtle_Stitch/Images/turtlestitch03.png)

Although Turtle Stitch might seem a little more basic than other tools (especially because the drawing functions are reduced to a few basic geometric shapes, which makes drawing sometimes a bit unprecise), I think this is a very interesting tool and I enjoyed exploiting its features and see how typography could emerge with this language and its interface. The project is not finished, the characters design must be continued to have a complete set and be able to write any text. But it seems to me that the prototypes that came out for now are already an original proposal and this deserves to be explored more deeply.