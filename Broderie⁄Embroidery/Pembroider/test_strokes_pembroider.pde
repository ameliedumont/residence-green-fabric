import processing.embroider.*;
PEmbroiderGraphics E;


void setup() {
  noLoop(); 
  size(2000, 1500);

  E = new PEmbroiderGraphics(this, width, height);
  
  E.beginDraw(); 
  E.clear();
  E.noFill(); 
  
 
  //-----------------------
  // Vary stroke weight, 
  // show both PERPENDICULAR & TANGENT
  int nLetters = 9; 
  
  //E.PARALLEL_RESAMPLING_OFFSET_FACTOR = 0.33;
  
  //E.strokeCap(SQUARE); // NOTE: currently not working for PERPENDICULAR
   
  
  //repeats a stitch at the end of each line drawn
  E.beginRepeatEnd(2);
  
  E.strokeSpacing(2.5);
  E.setStitch(5, 15, 0.0);
  E.strokeMode(PEmbroiderGraphics.PERPENDICULAR);
  int lineX = 60;
  int lineY = 60;
  for (int i=0; i<nLetters; i++) {
    E.setStitch(5+i, 15+i, 0.0);
    float x0 = lineX; 
    float x1 = x0+60;
    float y0 = lineY;
    float y2 = y0+240;
    float y3 = y0+160;
    E.strokeWeight(i*i); 
    E.ellipse(x0, y3, 120, 160);
    E.line(x1, y0, x1, y2);
    lineX += 200+i*i;
    
  }
  
  
  E.setStitch(5, 15, 0.0);
  E.strokeMode(PEmbroiderGraphics.TANGENT);
  E.strokeCap(SQUARE);
  lineX = 150;
  lineY = 370;
  for (int i=1; i<nLetters; i++) {
    E.strokeSpacing(i);
    float x0 = lineX; 
    float x1 = x0+60;
    float y0 = lineY+40;
    float y2 = y0+100;
    float y3 = lineY+100;
    float y4 = lineY+150;
    float y5 = lineY+230;
    E.strokeWeight(i*i); 
    E.ellipse(x0, y3, 120, 120);
    E.line(x1-60, y0, x1+40, y0);
    E.arc(x0-60, y4+i*2, 60+i*2, 60+i*2, PI+HALF_PI, HALF_PI);
    E.arc(x0-10, y5+i*2, 130, 130, 0, PI+QUARTER_PI, OPEN);
    lineX += 200+i*i;
  }
  
  E.strokeSpacing(2.5);
  E.setStitch(5, 15, 0.0);
  E.strokeMode(PEmbroiderGraphics.PERPENDICULAR);
  E.strokeCap(SQUARE);
  lineX = 60;
  lineY = 780;
  for (int i=0; i<nLetters; i++) {
    float x0 = lineX; 
    float x1 = x0+120;
    float y0 = lineY;
    float y2 = y0+120;
    E.strokeWeight(i*i);
    E.strokeSpacing(2.5+i);
    E.line(x0, y0, x1, y0);
    E.line(x1, y0, x0, y2+i);
    E.line(x0, y2+i, x1, y2+i);
    lineX += 200+i*i;
  }

  

  ////-----------------------
  //// Vary the strokeSpacing, 
  //// show both PERPENDICULAR & TANGENT
  //E.strokeCap(ROUND);
  //E.strokeWeight(15); 
  //E.setStitch(5, 15, 0.0);
  //E.strokeMode(PEmbroiderGraphics.PERPENDICULAR); 
  
  //lineX += lineLength+50;
  //for (int i=0; i<nLines; i++) {
  //  float x0 = lineX; 
  //  float x1 = x0+lineLength;
  //  float y0 = map(i, 0, nLines-1, 50, height-50);
  //  E.strokeSpacing(1.0 + i);
  //  E.line (x0, y0, x1, y0);
  //}
  
  
  //E.strokeCap(ROUND);
  //E.strokeWeight(15); 
  //E.setStitch(5, 15, 0.0);
  //E.strokeMode(PEmbroiderGraphics.TANGENT);
  //lineX += lineLength+50;
  //for (int i=0; i<nLines; i++) {
  //  float x0 = lineX; 
  //  float x1 = x0+lineLength;
  //  float y0 = map(i, 0, nLines-1, 50, height-50);
  //  E.strokeSpacing(1.0 + i);
  //  E.line (x0, y0, x1, y0);
  //}

  E.endRepeatEnd();

  //-----------------------
  //E.optimize(); // slow, but very good and important
  E.visualize();
  String outputFilePath = sketchPath("Pembroider_test.dst");
  E.setPath(outputFilePath); 

  E.endDraw(); // write out the file
  //save("Pembroider_test.svg");
}

void draw() {
  ;
}
