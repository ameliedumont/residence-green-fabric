import processing.embroider.*;
PEmbroiderGraphics E;


void setup() {
  noLoop(); 
  size(3000, 2500);

  E = new PEmbroiderGraphics(this, width, height);
  
  E.beginDraw(); 
  E.clear();
  E.noFill(); 
  
 
  //-----------------------
  // Vary stroke weight, 
  // show both PERPENDICULAR & TANGENT
  int nLetters = 11; 
  
  //E.PARALLEL_RESAMPLING_OFFSET_FACTOR = 0.33;
  
  //E.strokeCap(SQUARE); // NOTE: currently not working for PERPENDICULAR
   
  
  //repeats a stitch at the end of each line drawn
  E.beginRepeatEnd(2);
  
  
  int st = 50;
  int sw = 40;
  E.strokeMode(PEmbroiderGraphics.TANGENT);
  int lineX = 120;
  int lineY = 60;
  for (int i=0; i<nLetters; i++) {
    E.strokeSpacing(8+i);
    E.setStitch(st, st, 0.0);
    float x0 = lineX; 
    float x1 = x0+60;
    float y0 = lineY;
    float y2 = y0+240;
    float y3 = y0+210;
    E.strokeWeight(55); 
    E.ellipse(x0, y3, 120, 160);
    E.beginShape();
    E.vertex(x0, y0);
    E.quadraticVertex(x0+90, y0+025, x0+115, y0+280);
    //E.quadraticVertex(x0+125, y0+375, x0+200, y0+400);
    E.endShape();
    lineX += 250;
    st += 10;
  }
  
  int el = 80;
  float sl = 1.0;
  E.setStitch(30, 30, 0.0);
  E.strokeMode(PEmbroiderGraphics.PERPENDICULAR);
  E.strokeSpacing(8);
  E.strokeCap(SQUARE);
  lineX = 150;
  lineY = 450;
  for (int i=1; i<nLetters; i++) {
    float x0 = lineX; 
    float x1 = x0+60;
    float y0 = lineY+40;
    float y2 = y0+100;
    float y3 = lineY+100;
    float y4 = lineY+160;
    float y5 = lineY+260;
    E.strokeWeight(30); 
    E.strokeLocation(sl);
    E.ellipse(x0, y3, el, 120);
    E.line(x1-60, y0, x1+10*i, y0);
    E.arc(x0-60, y4+i*2, 60+i*2, 80+i*2, PI+HALF_PI, HALF_PI);
    E.strokeLocation(0.0);
    E.ellipse(x0, y5, el+10, 120);
    lineX += 220 +12*i;
    el += 20;
    sl -= 0.2;
  }
  
  int stw = 55;
  E.strokeSpacing(10);
  E.setStitch(50, 50, 0.0);
  E.strokeMode(PEmbroiderGraphics.PERPENDICULAR);
  //E.strokeCap(SQUARE);
  lineX = 60;
  lineY = 930;
  for (int i=0; i<nLetters; i++) {
    float x0 = lineX; 
    float x1 = x0+150;
    float y0 = lineY;
    float y1 = lineY+300;
    float y2 = y0+120;
    E.strokeWeight(stw);
    E.strokeSpacing(5+i*2);
    E.line(x0, y0, x0, y1+50*i);
    E.line(x1+i*10, y0, x1+i*10, y1+50*i);
    E.line(x0+30, y0, x1+i*10, y1+50*i);
    lineX += 250+30*i;
    stw += 12;
  }

  
  


  E.endRepeatEnd();

  //-----------------------
  E.optimize(); // slow, but very good and important
  E.visualize();
  String outputFilePath = sketchPath("Pembroider_test.dst");
  E.setPath(outputFilePath); 

  E.endDraw(); // write out the file
  //save("Pembroider_test.svg");
}

void draw() {
  ;
}
