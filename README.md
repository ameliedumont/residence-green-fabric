(ENGLISH BELOW)

# La typographie pour le tricot et la broderie numériques - Résidence chez Green Fabric 2023

Durant 6 semaines, j'ai eu accès au fablab textile Green Fabric à Bruxelles afin d'y mener un projet de recherche et d'expérimentation. Mon projet visait à explorer les moyens de penser et concevoir la typographie pour les machines à tricoter et à broder numériques du fablab. Vous trouverez dans les dossiers dédiés le compte-rendu des recherches que j'ai menées pour ces deux techniques textiles. Cette résidence a été rendue possible par le reseau des fablabs bruxellois.

---
# Typography for digital knitting and embroidery - 2023 Green Fabric residency

I could access the textile fablab Green Fabric in Brussels for 6 weeks to lead a project of research and experimentation there. My project was about thinking and designing typography for the digital knitting and embroidery machines in the fablab. You will find in the dedicated folders the documentation of my researches for both textile techniques. This residency was made possible thanks to the network of the Brussels fablabs.