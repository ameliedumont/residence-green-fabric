//initialisation of the canvas
var c = document.getElementById("knit");
var ctx = c.getContext("2d");
ctx.fillStyle = "white";
ctx.fillRect(0, 0, c.width, c.height);
ctx.imageSmoothingEnabled = false;

//change the width of the canvas (number of stitches)
function updateWidth() {
  c.width = document.getElementById("stitchW").value;
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, c.width, c.height);
}

//change the height of the canvas (number of stitches)
function updateHeight() {
  c.height = document.getElementById("stitchH").value;
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, c.width, c.height);
}

//this function gets the text typed in the text area and composes the text with the selected font
function makeText() {
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, c.width, c.height);
  var text = document.getElementById("text").value;
  var text_split = text.split("");
  var x_start = document.getElementById("Xpos").value;
  var y_start = document.getElementById("Ypos").value;
  var lineHeight = document.getElementById("lineHeight").value;
  var x_pos = 0 + parseInt(x_start);
  var y_pos = 0 + parseInt(y_start);
  var imgW = document.getElementById("imgW").value;
  var imgH = document.getElementById("imgH").value; 
  var textRotation = document.getElementById("textRotation");
  var imgRotation = textRotation.options[textRotation.selectedIndex].value;
  var fontSelection = document.getElementById("fontSelection");
  var fontChoice = fontSelection.options[fontSelection.selectedIndex].value;
  
  //we rotate the canvas to rotate the text (if a rotation is wanted)
  ctx.setTransform(1,0,0,1,0,0);
  if(imgRotation == "90") {
    ctx.translate(document.getElementById("stitchW").value, 0);
  } else if(imgRotation == "180") {
    ctx.translate(document.getElementById("stitchW").value, document.getElementById("stitchH").value);
  } else if(imgRotation == "-90") {
    ctx.translate(0, document.getElementById("stitchH").value);
  } else {
    ctx.translate(0, 0);
  }
  
  ctx.rotate((parseInt(imgRotation) * Math.PI) / 180);

  //we compose the text to knit by picking the characters in the text area one by one
  text_split.forEach((glyph) => {

      var img = new Image();

      img.onload = function() {

      //go to new line when a line break character is typed
      if(glyph == "\n") {
        x_pos = (0 + parseInt(x_start)) - (img.width * imgW) - (2 * imgW);
        y_pos += parseInt(lineHeight);
      }
      
      //scaling of the text (none if scale values equal 1)
      if(img.width * imgW > c.width) {
        c.width = img.width * imgW;
      } 
      if(img.height * imgH > c.height) {
        c.height = img.height * imgH;
      }
        
      //this prevents rasterisation of the images
      ctx.imageSmoothingEnabled = false;
      
      //here we define kernings and letter spacing exceptions for the different fonts
      if(fontChoice == "pixel") {
        if(glyph == "s") {
          ctx.drawImage(img, x_pos-2, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) + (1 * imgW);
        } else {
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) + (2 * imgW);
        }
      } else if(fontChoice == "anglaise") {
        if(glyph == "l") {
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) - (5 * imgW);
        } else if(glyph == "d") {
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) - (4 * imgW);
        } else if(glyph == "T") {
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) - (10 * imgW);
        } else if(glyph == "h") {
          x_pos -= (1 * imgW);
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW);
        } else if(glyph == "u") {
          x_pos -= (3 * imgW);
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) + (2 * imgW);
        } else if(glyph == "o") {
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) - (2 * imgW);
        } else if(glyph == "i" ) {
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) - (1 * imgW);
        } else if(glyph == "v" || glyph == "w") {
          x_pos -= (1 * imgW);
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) - (1 * imgW);
        } else if(glyph == "n") {
          x_pos -= (1 * imgW);
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) + (2 * imgW);
        } else if(glyph == "t") {
          x_pos -= (1 * imgW);
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) - (2 * imgW);
        } else if(glyph == "r") {
          x_pos -= (6 * imgW);
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) + (3 * imgW);
        } else if(glyph == "s") {
          x_pos -= (5 * imgW);
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) + (2 * imgW);
        } else if(glyph == "p") {
          x_pos -= (6 * imgW);
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) + (2 * imgW);
        } else if(glyph == "f") {
          x_pos -= (7 * imgW);
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) - (5 * imgW);
        } else if(glyph == "g" || glyph == "y") {
          x_pos -= (7 * imgW);
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) + (1 * imgW);
        }else {
          ctx.drawImage(img, x_pos, y_pos, img.width * imgW, img.height * imgH);
          x_pos += (img.width * imgW) + (2 * imgW);
        }
        
      }

      
      
    }

    //here we define rules for replacing each typed character by the correct image in the font folder
    if(fontChoice == "pixel") {
      if(glyph == " ") {
        img.src = 'images/pixel/space.png';
      } else if(glyph == "\n") {
        img.src = 'images/pixel/space.png';
      } else if(glyph == ".") {
        img.src = 'images/pixel/point.png';
      } else if(glyph == "'") {
        img.src = 'images/pixel/apostrophe.png';
      } else if(glyph == "-") {
        img.src = 'images/pixel/dash.png';
      } else {
        img.src = 'images/pixel/' + glyph + '.png';
      }

    } else if(fontChoice == "anglaise") {
      if(glyph == " ") {
        img.src = 'images/anglaise/space.png';
      } else if(glyph == "\n") {
        img.src = 'images/anglaise/space.png';
      } else {
        img.src = 'images/anglaise/' + glyph + '.png';
      }
    }


  });

}


//export the content of the canvas as a PNG image
function exportCanvas(){ 
  var mycanvas = document.getElementById("knit");
  if(mycanvas && mycanvas.getContext) {
    var img = mycanvas.toDataURL("image/png;base64;");
    anchor = document.getElementById("download");
    anchor.href = img;
    anchor.innerHTML = "Download";
  }
}