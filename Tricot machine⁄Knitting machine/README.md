(ENGLISH BELOW) 

# Tricot machine

Le fablab Green Fabric à Bruxelles dispose me machines à tricoter hackées. Il s'agit de machines à tricoter domestiques datant des années 80, ces machines étaient électroniques : elles disposaient de motifs pré-enregistrés que l'on pouvait tricoter, les aiguilles à tricoter étaient alors contrôlées par le dispositif électronique de la machine afin de croiser deux fils de couleurs différentes et composer le motif choisi ligne par ligne. Il est possible de hacker ces machines en branchant sur les connectiques d'origine une carte Arduino spéciale (c'est le projet [AYAB](https://ayab-knitting.com/)). On peut alors connecter son ordinateur à cet Arduino et envoyer ses propres motifs à tricoter à la machine.

L'utilisation d'AYAB impose des contraintes :

1. AYAB accepte des images comme motifs à tricoter. Ces images doivent être en noir et blanc et utiliser une palette de couleurs 1-bit (Bitmap). On peut tricoter avec deux fils simultanément sur la machine afin de produire des motifs ; les pixels blancs de notre image seront tricotés avec le premier fil et les pixels noirs avec le second.
2. Pour AYAB, 1 pixel de l'image = 1 maille du tricot. Notre image doit donc comporter autant de pixels en largeur et en hauteur que le tricot.

Pour préparer ses motifs à tricoter, on travaille donc avec une unité de base qui est la maille tricotée. Dans le cas de mon projet qui vise à penser la typographie pour cette technique textile, cela pose des questions en terme de dessin de caractères. 

J'ai fait un premier essai de texte composé dans GIMP, directement au format du tricot (le même nombre de pixels en largeur et en hauteur que le nombre de mailles du tricot). La typographie utilisée est Liberation Serif, composée en corps 28, 14 et 7. On voit ci-dessous le rendu de ce fichier dans GIMP :

![Gimp](Images/tricot0.png)

Voici ce que donne cette image une fois tricotée :

![Liberation Serif tricotée](Images/tricot01.png)

On perçoit ici un problème : en procédant de cette façon, la typographie subit une rastérisation (conversion en image matricielle) sur laquelle on n'a pas de contrôle. GIMP va gérer la conversion des contours des lettres en pixels noirs ou blancs en fonction de ses propres algorithmes. Ce qui résulte en une conversion quelque peu aléatoire des contrastes de la typographie, et ce problème est accentué à plus petite échelle. D'ailleurs on peut voir que si le corps du texte est trop petit, les lettres disparaissent complètement car les mailles constituent une unité de base trop grosse pour la définition de la typographie à un petit corps.

Suite à cette première expérience, j'ai décidé d'orienter ma recherche sur le tricot en prenant en compte les problématiques soulevées : comment et avec quels outils dessiner de la typographie pour la machine à tricoter en prenant en compte la contrainte de l'unité de base qu'est la maille ? Comment passer du pixel à la maille sans perte ou déformation de l'information ? Comment gérer la direction dans laquelle le texte va être tricoté ? Car une maille tricotée n'a pas la forme d'un pixel carré, tricoter à l'horizontale ou à la verticale change l'aspect du motif (on le voit sur l'image ci-dessus).

## Libre Office Calc

Le premier outil avec lequel j'ai conçu de la typographie pour le tricot est [Libre Office Calc](https://fr.libreoffice.org/discover/calc/). Il s'agit d'un tableur, ce qui permet de travailler avec des cellules arrangées en lignes et en colonnes. Ceci correspond assez bien à un motif tricoté, qu'on peut également décomposer en lignes et colonnes elles-mêmes composées de cellules (mailles). En travaillant avec Libre Office Calc, on peut faire correspondre une cellule du tableur avec une maille du tricot, ce qui permet de maitriser précisément le rendu de son motif.

J'ai arrangé l'outil de manière à faciliter la visualisation des lettres dessinées, en reprenant un setup mis en place par Stéphanie Vilayphiou pour un [workshop tricot machine](https://greenfabric.be/workshop-generation-de-motifs-machine-a-tricoter/) organisé à Green Fabric en 2021.

Il faut :

1. Mettre toutes les cases en carreaux de 5mm. Pour cela, il faut cliquer sur le nom des colonnes (A, B, C...) puis sur **Largeur de colonnes** et là entrer la valeur "5mm". Idem pour les lignes, cliquer sur le nom des lignes puis sur  **Hauteur de lignes** et entrer "5mm" comme valeur.
2. Ensuite on va mettre en place un formatage conditionnel qui donnera un fond noir à toutes les cases contenant la valeur "1" et un fond blanc à celles qui contiennent la valeur "0". Pour cela, il faut aller dans **Format** > **Formatage conditionnel** > **Gérer**. Dans la fenêtre qui s'ouvre, cliquer sur **Ajouter**. Dans **Condition 1**, entrer pour valeur de la cellule "1" et pour style un fond noir. Pour la condition 2, entrer pour valeur de la cellule "0" et pour style un fond blanc. Cela doit ressembler à ceci :

![Condition 1 Libre Office Calc](Images/calc07.png)

![Condition 2 Libre Office Calc](Images/calc08.png)

Une fois ce formatage fait on peut commencer à dessiner des lettres en remplissant de 1 et de 0 les cases du tableur. L'intérêt de travailler dans un tel outil est qu'on peut déformer les caractères en venant ajouter des lignes et des colonnes dedans, comme on peut le voir ci-dessous :

![Libre Office Calc](Images/calc04.png)

J'ai ensuite réutilisé et modifié un script écrit par Stéphanie Vilayphiou pour ce même workshop, qui permet d'exporter le contenu de son tableur vers un fichier image, pour ensuite pouvoir le fournir à AYAB et le tricoter. Avec cette méthode, on est sûr.e.s que son motif en pixels sera transposé à la même échelle lors de la conversion en image, et donc que le motif pensé avec les cellules sera rendu de manière égale dans les mailles du tricot.

Il s'agit du script **csv2xpm.py**. Pour l'utiliser, il faut commencer par exporter son tableur au format CSV (un format de fichier dans lequel les données sont séparées par des virgules). On peut faire cela en allant dans **Fichier** > **Enregistrer une copie**, et là sauver son fichier avec l'extension CSV.

*Attention pour que le script fonctionne, il faut que les cases vides du motif contiennent la valeur "0". Le plus simple est de remplir entièrement son tableur de "0" en commençant le travail, puis de les remplacer par des "1" dans les cellules qui vont dessiner le motif. Sinon, on peut faire un rechercher/remplacer dans son fichier CSV pour remplacer automatiquement la valeur des cellules vides par des "0"*

Ensuite il faut placer le fichier CSV dans le même dossier que le script et le lancer dans un terminal avec la commande suivante (remplacer *mon_fichier.csv* par le nom de son fichier) :

    > python3 csv2xpm.py mon_fichier.csv

Le script va convertir le fichier en pixels pour faire une image au format XPM. Ce format permet d'obtenir une image déjà en mode Bitmap.  On peut voir une prévisualisation de son fichier dans le terminal lorsque le script fonctionne.

![Libre Office Calc](Images/calc05.png)

![Libre Office Calc](Images/calc06.png)

Il reste une dernière étape avant de pouvoir tricoter ses lettres dessinées dans un tableur : ouvrir le fichier XPM que le script vient de générer dans un logiciel de modification d'images (GIMP par exemple), et exporter l'image au format PNG.

À partir de là, on va travailler sur la machine à tricoter hackée. Si vous possédez votre propre machine, voir [la documentation d'AYAB](https://manual.ayab-knitting.com/0.95/) pour l'installation matérielle et logicielle nécessaire. Une fois le logiciel AYAB, lancé, on se retrouve dans cette interface : 

![AYAB software](Images/ayab01.png)

Il faut brancher son ordinateur à l'Arduino sur lequel AYAB est installé, importer son image, vérifier la configuration, puis lancer le tricot en cliquant sur "3. Knit!". Le logiciel va donner des indications au début du tricot sur les manipulations à effectuer sur la machine.

![Machine à tricoter hackée](Images/machine.jpeg)

Voici les tricots obtenus avec les typographies dessinées dans Libre Office Calc :

![Libre Office Calc](Images/calc01.JPG)

![Libre Office Calc](Images/calc02.JPG)

![Libre Office Calc](Images/calc03.JPG)

![Libre Office Calc](Images/tricot02.png)

Cette première étape de recherche avec un tableur a permis de faire émerger de premières formes typographiques pensées en pixels, et ce à différentes échelles. Cependant cet outil reste assez rigide et limité, je le recommande pour faire des lettrages ou prototyper ses typographies, mais la composition de texte dans un tableur est très fastidieuse et peu pratique. Il est est également complexe de faire pivoter son texte si on veut tricoter dans un autre sens.

C'est pourquoi j'ai décidé de développer mon propre outil de composition de texte pour le tricot, à partir de typographies conçues en pixels.

## Canvas to knit, un outil sur-mesure

Sachant que pour le tricot on a besoin d'exporter des images et que le web permet de créer des interfaces de composition de texte interactives assez simplement, la solution pour mêler les deux m'a semblé être les canvas. Ce sont des éléments HTML qui permettent de dessiner en pixels dans une page web à l'aide du langage Javascript. On définit des instructions de dessin dans un script, qui seront transformées en un motif en pixels dans le canvas. Les possibilités de dessin sont assez vastes pour couvrir les besoins du projet, on peut notamment appliquer des déformations d'échelle sur les motifs ou les faire pivoter. De plus il est possible d'exporter des images au format PNG directement depuis un canvas HTML (c'est quelque chose que j'avais déjà fait pour le festival [Lesbiennale](https://gitlab.com/ameliedumont/workshop-esad-pau/-/tree/main/Exemples/Lesbiennale%20post%20generator?ref_type=heads) en 2021). Cela me semblait donc être un bon choix pour composer du texte et l'exporter sous forme d'image pour le tricot.

J'ai donc commencé à développer une interface web avec un canvas et des champs texte/menus déroulants permettant de modifier les paramètres à prendre en compte dans le tricot. On peut modifier avec cet outil :

- le nombre de mailles en largeur et en hauteur
- l'échelle du texte en largeur et en hauteur (les deux sont dissociés, on peut déformer les typographies)
- à quelles mailles faire commencer le motif en haut et à gauche
- l'interlignage
- la rotation à appliquer sur le texte (4 possibilités : 0°, 90°, 180° et -90°) pour pouvoir choisir selon quel sens des mailles on veut tricoter
- la typographie avec laquelle composer le texte (l'outil à l'heure actuelle comporte deux jeux de typographie non complets, mais vous pouvez dessiner vos propres typographies et les incorporer)
- le texte à composer. Les retours à la ligne sont reconnus dans ce champ texte, ce qui permet d'écrire sur plusieurs lignes.

Pour utiliser l'outil, il faut cloner le dossier **Canvas_to_knit**. Puis déplacez-vous dans ce dossier et ouvrez un terminal. Pour lancer l'outil, il faut lancer un serveur local avec la commande : 

    > python3 -m http.server
    
Le terminal va alors vous indiquer sur quel port tourne le serveur (en général 8000). Ouvrez un navigateur et tapez l'URL "localhost:8000" (ou autre si le port est différent). Vous devez alors voir cette interface web :

![Canvas to knit](Images/webtoknit05.png)

L'outil permet en l'état de composer du texte et de modifier le dessin avec les typographies que j'y ai intégrées. La première typographie est une fonte en pixels très simple, dont la graisse de base équivaut à un pixel (ou une maille). J'ai dessiné les lettres dans Libre Office Calc afin de m'assurer d'une certaine régularité entre les caractères :

![Pixel font](Images/webtoknit08.png)

Chaque lettre est ensuite exportée sous forme d'image, et ces lettres sont regroupées dans un dossier par typographie dans les fichiers de l'outil (voir le dossier **images** de l'outil pour comprendre). Ce qui se produit est que, quand on compose du texte, le script de l'outil va aller regarder quels sont les caractères (et dans quelle typographie) à utiliser, puis les faire apparaitre les uns à la suite des autres pour recomposer le texte dans le canvas. On travaille donc avec des images des lettres, dans lesquelles le dessin des caractères a été pensé pour être rendu en pixels/mailles. L'import des images dans le canvas ne va pas leur faire subir de rastérisation, le rendu des lettres dans l'image finale sera donc le même qu'à l'écran, et ce même si on agrandit l'échelle du texte. J'ai mis en place quelques kernings dans le script afin certains espacements disgracieux dans la composition. Les retours à la ligne sont à gérer manuellement, en pressant la touche **Entrée**.

*Si vous souhaitez ajouter vos propres typographies à l'outil, attention à bien travailler avec une ligne de base et à exporter les images de toutes les lettres à la même hauteur (qui va des ascendantes aux descendantes), sinon vous aurez des décalages lors de la composition du texte*

Voici ce que donne cette typographie pixel tricotée à l'échelle 1:1, avec un extrait du texte *Turing complete user* d'Olia Lialina :

![Canvas to knit](Images/webtoknit01.JPG)

Et la même typographie tricotée à l'échelle 2:3 et pivotée à 90° (afin d'exploiter la forme irrégulière des mailles et jouer sur la déformation des lettres), avec un autre extrait du même texte :

![Canvas to knit](Images/webtoknit02.JPG)

![Canvas to knit](Images/webtoknit03.JPG)

Après avoir mis en place cette première version basique mais tout de même fonctionnelle de l'outil, j'ai voulu aller plus loin en terme de dessin de caractères. Travailler sur des formes plus complexes et à plus grande échelle. C'est ainsi que j'ai commencé à dessiner une typographie pixel basée sur la calligraphie anglaise. J'ai voulu travailler ces caractères là aussi dans Libre Office Calc, à une échelle permettant de rendre les variations des pleins et des déliés :

![Anglaise](Images/webtoknit09.png)

Puis j'ai procédé de la même façon que pour la première typographie pour intégrer ces nouveaux caractères dans l'outil de composition. J'ai à cette occasion dû modifier mon script afin d'adapter les approches et les kernings pour ces lettres en italique. Voici ce que donne cette typographie dans l'outil :

![Canvas to knit](Images/webtoknit04.png)

Le résultat de ce tricot, à l'échelle 1:1 :

![Web to knit](Images/webtoknit07.JPG)

![Web to knit](Images/webtoknit06.JPG)

Un tricot de la même typographie, sans rotation et à l'échelle 1:1. On peut voir comment le sens dans lequel le texte est tricoté change les proportions des lettres :

![Web to knit](Images/tricot03.png)

Je publie cet outil au terme de la résidence dans une première version qui demande à être améliorée et enrichie. Malgré ses limites, ce petit projet a eu le mérite de proposer des solutions aux problèmes identifiés en début de résidence par rapport au tricot de lettres, à savoir :

1. Empêcher la rastérisation hasardeuse
2. Préserver le ratio du dessin des caractères même à différentes échelles
3. Pouvoir composer du texte avec des lettres en pixels et choisir le sens du texte sur le tricot

Ce projet m'a donné envie de poursuivre le dessin de caractères en pixels, pour toutes les questions de design que cela pose, et peut-être comme une ouverture pour la suite d'explorer plus largement par quels autres moyens on pourrait mêler web et tricot.

—

# Machine knitting

The fablab Green Fabric in Brussels has hacked knitting machines. These are domestic knitting machines from the 80s' that were electronic : they have a set of pre-recorded patterns that could be knitted, the needles were then controlled by the electronic device in the machine in order to knit the pattern with threads in two different colors line by line. It is possible to hack these machines by pluging a special Arduino board (this is the [AYAB](https://ayab-knitting.com/) project) on the original electronic connections. It is then possible to plug your laptop to this Arduino board and send your own patterns to the knitting machine.

the use of AYAB is constrained by :

1. AYAB accepts pictures as an input for knitting patterns. The pictures must be black and white and use a 1-bit (Bitmap) color mode. It is possible to knit with two threads at the same time to create patterns; white pixels will be knitted with the first thread and black pixels with the second one. 
2. For AYAB, 1 pixel from the image = 1 stitch from the knit. So the picture must be as wide and as high in pixels as the number of stitches expected for the knit.

When we prepare patterns for machine knitting, we work with a basic unity which is the stitch. As my project was about thinking typography for the textile technique, this has to be taken into account in the type design work. 

I made a first test of text, with a typesetting done in GIMP, the file was as large and high in pixels as the knit would be. The font used is Liberation Serif, here with font sizes 28, 14 and 7. We can see below the rendering of this file in GIMP:

![Gimp](Images/tricot0.png)

And this is what this image was knitted into:

![Liberation Serif tricotée](Images/tricot01.png)

We can see a problem here: by preparing the file this way, the typography is rasterised (converted into a pixel file) in a way that can't be controlled. GIMP manages the conversion of the letters outlines into black or white pixels according to its own algorithms. This results into a bit of a random conversion, especially on the contrasted parts of the letters, and it becomes worse if we lower the font size. We can see on the knit above that some letters almost disappeared if the font size was too small, because the stitch as a basic unit was too big to render a font that size.

After this first experience, I decided to guide my knitting research towards tracks that would take into account the issues that were just raised: how and with which tools can I draw typography for machine knitting considering the constraint of the stitch as a basic unit? How to jump from the pixel to the stitch without losing or corrupting the information on the way? How to modify the direction into which the text will be knitted? Because a knitted stitch does not have the shape of a squared pixel and knitting vertically or horizontally changes the aspect of the pattern (we can see it in the test above).

## Libre Office Calc

The first tool I used for this research is [Libre Office Calc](https://www.libreoffice.org/discover/calc/). It is a spreadsheet software, which enables to work with cells arranged in rows and columns, and for this reason this tool matches with a knitted pattern, which can also be decomposed into lines and columns, themselves made of cells (stitches). By working with Libre Office Calc, we can match one cell of the spreadsheet with one stitch from the knit and have a control on how the pattern will be rendered.

I made a little arrangement in the tool to make the visualisation of the letters drawn inside easier, by re-using a setup by Stéphanie Vilayphiou for a [knitting machine workshop](https://greenfabric.be/workshop-generation-de-motifs-machine-a-tricoter/) that took place at Green Fabric in 2021.

To do this, you have to :

1. Modify the cells to make it look like 5mm squares. To do so, click on the colun names (A, B, C...), then on **Columns width** and set it to "5mm". Same for the lines, click on the lines names, then on  **Lines height** and set it to "5mm".
2. Then, we are going to set a conditional formatting that will give a black background to the cells with the value "1" and a white background to those with the value "0". To do so, go into **Format** > **Conditional formatting** > **Manage**. In the window that just opened, click on **Add**. In **Condition 1**, set "1" as the value of the cell and black as background style. For condition 2, set "0" as the cell value and white as the background style. It must look like this:

![Condition 1 Libre Office Calc](Images/calc07.png)

![Condition 2 Libre Office Calc](Images/calc08.png)

Once the spreadsheet is properly formatted we can start drawing letters by filling the cells with 0 and 1. What's also interesting with working this way is that we can distort the glyphs by inserting columns or rows inside, as we can see below:

![Libre Office Calc](Images/calc04.png)

Then I reused and modified a script written by Stéphanie Vilayphiou for the same workshop, which enables to export the content of the spreadsheet into an image file, so that we can then use it with AYAB and knit it. With this process we are sure the our pixel pattern will be transposed at the same scale when converted into an image, and that the pattern designed with the cells will be the same in AYAB.

This is the **csv2xpm.py** script. To use it, we must first export the spreadsheet in CSV (a file format that stores data separated with commas). We can do this by going to **File** > **Save a copy**, and save a new version of the file with CSV format.

*Attention: if you want the script to work, every blank cells in the pattern must contain the value "0". The easiest way to do it is to completely fill the spreadsheet with "0" before you start drawing lettters, then to replace by "1" the cells you want. Or you can do a search/replace in the CSV file and automatically replace the value of empty cells by "0"*

The you have to put the CSV file in the same folder as the script and run it from a terminal by using the following command (replace *my_file.csv* with the name of your file) :

    > python3 csv2xpm.py my_file.csv

The script will convert the file in pixels to create a XPM image. This format enables to have an image that is already in Bitmap mode. We can see a pre-visualisation of the file in the terminal while the script is running.

![Libre Office Calc](Images/calc05.png)

![Libre Office Calc](Images/calc06.png)

There is one last step before we can knit the letters designed in the spreadsheet: open the XPM file with an image manipulation software (GIMP for instance) and export it as a PNG file.

From now on, the rest of the work is connected to the knitted machine. If your have your own machine, see [AYAB's documentation](https://manual.ayab-knitting.com/0.95/) to know how to do the setting, for both material and software parts. Once AYAB is running, we see this interface:

![AYAB software](Images/ayab01.png)

Your laptop must be plugged in USB to the Arduino board on which AYAB is running, then import your image into AYAB, check the configuration, and start knitting by clicking the "3. Knit!" button. The software is going to give you guidelines on the manipulations to make on the machine when the knit work starts.

![Machine à tricoter hackée](Images/machine.jpeg)

Here are the knits I got with Libre Office Calc:

![Libre Office Calc](Images/calc01.JPG)

![Libre Office Calc](Images/calc02.JPG)

![Libre Office Calc](Images/calc03.JPG)

![Libre Office Calc](Images/tricot02.png)

This first step of the research with spreadsheets was useful to enable first pixel typographic shapes to come to the surface, and this at different scales. Nevertheless this approach remains quite rigid and limited, I recommand it for lettering or for prototyping fonts, but typesetting in a spreadsheet is really not practical and tedious. It is also complicated to rotate a text if you want to knit in another direction. 

This is why I chose to develop my own tool, that could manage more complex typesetting from typographies designed in pixels.

## Canvas to knit, a custom made tool

Considering that we need to export images for machine knitting and that the web enables to create quite easily interactive interfaces, I thought I could find a solution for both problems into canvas. These are HTML elements that can be used to draw in pixels inside of a web page with the language Javascript. We define drawing instructions in a script, that will be turned into a pattern made of pixels inside of the canvas. The possibilities in terms of drawing are vast enough to cover what I was looking for in this project, we can for instance modify the scale, distort or rotate shapes. Moreover, it is possible to export images as PNG files straight from a canvas in an HTML page (this is something I had already done for the festival [Lesbiennale](https://gitlab.com/ameliedumont/workshop-esad-pau/-/tree/main/Exemples/Lesbiennale%20post%20generator?ref_type=heads) in 2021). So the canvas appeared to me as a good choice for typesetting and export an image file of it for knitting.

So I started developing a web interface including a canvas and interactive text areas/scrolling menus used to modify the parameters we need to take into account for machine knitting. With this tool we have control on:

- the number of stitches, vertically and horizontally
- the text scale, both vertically and horizontally (they are dissociated, which enables distortion)
- on which stitches we want the pattern to start, both vertically and horizontally
- the line spacing
- the rotation to apply on the text (4 possibilities : 0°, 90°, 180° and -90°) to be able to choose the direction the text will be knitted in
- the pixel typography we want to use (for now the tool implements two incomplete typesets, but you can draw your own pixel fonts and add it to the tool)
- the texte we want to write. Line breaks are recognised in the text area, so we can write on several lines. 

To use this tool, clone the **Canvas_to_knit** repository. Then go into this folder and open a terminal. To run the tool you need a local server that you can launch with this command: 

    > python3 -m http.server
    
The terminal will tell you on which port the local server is running (generally 8000). Open a web browser and go to the URL "localhost:8000" (or else if another port is used). This is the interface you must see in your browser:

![Canvas to knit](Images/webtoknit05.png)

The tool as it is now enables typesetting and to modify the canvas with two typographies I integrated. The first font is a very simple pixel font, the basic font weight is one pixel (or one stitch). I designed the letters in Libre Office Calc to make sure the design of the different glyphs would be regular:

![Pixel font](Images/webtoknit08.png)

Then each letter is exported as an image file, and those letters are gathered in one folder per typography into the tool's files (take a look at the **images** folder in the tool to understand). What happens is that, when we do typesetting with the tool, the script is going to check which letters (and from which typography) are needed, then make them appear one after the other to finally compose the text into the canvas. So we work with images of the letters, in which the letter design was thought to be rendered in pixels/stitches. The import of the images in the canvas doesn't rasterise them, and their rendering in the final image is the same as on screen, and this even if the text is scaled. I've setup a few kernings in the script to avoid some unpretty spacings. The line breaks have to be done manually by pressing the **Enter** key.

*If you want to add your own fonts to the tool, you should work with a baseline and export all the letters images with the same height (that goes from the ascenders to the descenders), otherwise you will find yourself with gaps in height between your letters when doing typesetting*

This is how this pixel typography looks once knitted at scale 1:1. The text is an extract from *Turing complete user* by Olia Lialina :

![Canvas to knit](Images/webtoknit01.JPG)

And the same typography knitted at scale 2:3 and rotated from 90° (to benefit from the irregular shape and stitches and play with letters distortions), with another extract from the same text:

![Canvas to knit](Images/webtoknit02.JPG)

![Canvas to knit](Images/webtoknit03.JPG)

After setting up this first version (basic but functional anyway) of the tool, I wanted to go further on the type design part of it and work on more complex shapes on a bigger scale. This is how I started designing a pixel font based on english calligraphy letters. I also decided to work with Libre Office Calc, at a scale that was big enough to render the stroke contrasts in the letters:

![Anglaise](Images/webtoknit09.png)

Then the process was the same as before to integrate this new typography in the typesetting tool. This time I had to modify the script to adapt the letter spacing and kernings for these italic letters. This is how this typography looks in the tool:

![Canvas to knit](Images/webtoknit04.png)

The result of this image knitted at scale 1:1 and rotated 90°:

![Web to knit](Images/webtoknit07.JPG)

![Web to knit](Images/webtoknit06.JPG)

Another knit of the same typography, without rotation at scale 1:1. We can see how the direction of the text on the knit modifies the letters proportions:

![Web to knit](Images/tricot03.png)

I publish this tool in the end of my residency as a first version that deserves to be improved and enriched. But despite its limits, this project brought a proposal of solutions for issues relative to type knitting that were identified at the beginning of the residency, these issues were:

1. Prevent random text rasterisation
2. Preserve the letters ratio even at different scales
3. Find ways to do typesetting with pixel letters and be able to choose the direction in which the text will be knitted

This project made me want to go on designing pixel fonts, for the very interesting type design issues it raises. And maybe as an opening for later I also would like to explore other ways of putting together web tools and knitting.